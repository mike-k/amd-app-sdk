# AMD App SDK Installer

## About

This repo mirrors the files found at https://developer.amd.com/amd-accelerated-parallel-processing-app-sdk/.

I couldn't find them anywhere that they could be easily downloaded through a script.

## Safety

Please be sure to double check the md5 sum of the files downloaded against the official ones listed by AMD at https://developer.amd.com/amd-accelerated-parallel-processing-app-sdk/system-requirements-driver-compatibility/.

The AMD License Agreement: https://developer.amd.com/amd-license-agreement-appsdk/

### Usage

Below is an example showing downloading the file, unpacking it, and running the installer without interaction<sup>1</sup>.

```
wget https://gitlab.com/mike-k/amd-app-sdk/raw/master/AMD-APP-SDKInstaller-v3.0.130.136-GA-linux64.tar.bz2
tar -xjf AMD-APP-SDKInstaller-v3.0.130.136-GA-linux64.tar.bz2
./AMD-APP-SDK-*.sh -- --acceptEULA 'yes' -s
```

<sup>1</sup> Please make sure to read through the EULA at least once.
